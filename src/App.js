import React, { Component } from 'react';
import './App.css';
import ValidationComponent from "./ValidationComponent";
import CharComponent from "./CharComponent";

class App extends Component {

  state = {
    text: '',
    length: 0,
    chars: []
  }

  changeInputText = (event) => {
    let text = event.target.value;
    this.setState({
      text: text,
      length: text.length,
      chars: text.split("")
    })
  }

  showLineLength = () => {
    const text = (this.state.length === 1) ? 'a char' : this.state.length + ' chars'
    return (
      <div className='showLineLength'>
        Input text has {text}
      </div>
    )
  }

  onClickRemove = (index) => {
    const chars = [...this.state.chars]
    chars.splice(index, 1)

    this.setState({
      text: chars.join(""),
      length: chars.length,
      chars: chars
    })

  }

  showChars = () => {
    if (this.state.length > 0) {
      const charContent = this.state.chars.map((char, index) =>
          <CharComponent
            key={index}
            content={char}
            clicked={() => this.onClickRemove(index)}
          />
      )
      return (
        <div>
          {charContent}
        </div>
      )
    } else {
      return (<div></div>)
    }
  }

  render() {
    return (
      <div className="App">
        <ol>
          <li>Create an input field (in App component) with a change listener which outputs the length of the entered text below it (e.g. in a paragraph).</li>
          <li>Create a new component (=> ValidationComponent) which receives the text length as a prop</li>
          <li>Inside the ValidationComponent, either output "Text too short" or "Text long enough" depending on the text length (e.g. take 5 as a minimum length)</li>
          <li>Create another component (=> CharComponent) and style it as an inline box (=> display: inline-block, padding: 16px, text-align: center, margin: 16px, border: 1px solid black).</li>
          <li>Render a list of CharComponents where each CharComponent receives a different letter of the entered text (in the initial input field) as a prop.</li>
          <li>When you click a CharComponent, it should be removed from the entered text.</li>
        </ol>
        <p>Hint: Keep in mind that JavaScript strings are basically arrays!</p>
        <hr />
        <div className='inputText'>
          <input type="text" onChange={(event) => this.changeInputText(event)} value={this.state.text} />
        </div>
        {this.showLineLength()}
        <ValidationComponent textLength={this.state.length} />
        {this.showChars()}
      </div>
    );
  }
}

export default App;
