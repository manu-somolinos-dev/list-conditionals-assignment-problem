import React from 'react'

const charComponentStyle = {
  display: 'inline-block',
  padding: '16px',
  textAlign: 'center',
  margin: '16px',
  border: '1px solid black'
}

const CharComponent = (props) => {
  return (
    <div style={charComponentStyle} onClick={props.clicked}>
      {props.content}
    </div>
  )
}

export default CharComponent
